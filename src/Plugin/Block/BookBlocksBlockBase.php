<?php

namespace Drupal\book_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;

/**
 * Provides bas class for another 'Book navigation' block.
 */
abstract class BookBlocksBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The book manager.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected $bookManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Get node URL from nid.
   *
   * @param int $nid
   *
   * @return string
   */
  public function node_url($nid) {
    return Url::fromRoute('entity.node.canonical', ['node' => $nid])->toString();
  }

  /**
   * Load node URL from nid.
   *
   * @param int $nid
   *
   * @return node
   */
  public function node_load($nid) {
    return \Drupal::entityTypeManager()->getStorage('node')->load($nid);
  }

  /**
   * Add class to identify current node in list.
   *
   * @param $list array
   *   Hierarchical list of book elements
   * @param $nid integer
   *   Book page node ID
   */
  public function addClassForCurrentNode(&$list, $nid) {
    if ($list) {
      foreach ($list as $key => $item) {
        if (is_array($item) && isset($item['url']) && $item['url']->getRouteParameters()['node'] == $nid) {
          $list[$key]['attributes']->setAttribute('class', 'book-blocks-active');
          return;
        }

        if (isset($item['below'])) {
          $this->addClassForCurrentNode($list[$key]['below'], $nid);
        }
      }
    }
  }

}
